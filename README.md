# Valheim Content - Made in Germany

Mod/Content Collection made by Eihwaz´n´Friendz.

--- --- --- --- --- --- --- --- ---

**Table of Contents**

**Hotkeys** ([**Preview**](https://www.eihwaz.de))

[AutoHotkey](https://autohotkey.com/download/) EXE to execute 1-click emotes, dice rolls and coin flips. Addtional 1-click whisper/scream buttons.

- [x] **Deutsch**
- [x] **English**

Num0 = sit / Num1 = wave / Num2 = cheer / Num3 = thumbsup / Num 4 = point / Num5 = challenge / Num6 = nonono

Num7 = dice roll / Num8 = coin flip / Num9 = special greeting / NumEnter = scream / NumAdd = whisper
 
--- --- --- --- --- --- --- --- ---

**Copyrights: Attribution-NonCommercial-NoDerivatives 4.0 International**

You are allowed to use this mod for your private use (singleplayer/multiplayer/server).

You are not allow to reupload it without permission!

You are not allow to charge money for it!
